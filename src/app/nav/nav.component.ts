import { Component, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../user/login/auth.service';
import { ISession, EventService } from '../events';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav.component.html',
  styles: [`
    button.active { color: orange }

    .flex-container {
      display: flex;
    }
  `
  ]
})
export class NavBarComponent {
  searchTerm = '';
  foundSessions: ISession[] = [];
  @ViewChild('modalContainer') containerContentModal: ElementRef;
  @ViewChild('modalTitle') containterTitleModal: ElementRef;

  constructor(public authService: AuthService, private eventService: EventService) {}

  searchSessions(searchTerm: string) {
    this.eventService.searchSessions(searchTerm).subscribe(sessions => {
      this.foundSessions = sessions;
    });
  }
}
