import { Directive, OnInit, Inject, ElementRef, Input } from '@angular/core';
import { JQUERY_TOKEN } from '../jQuery.service';
import { SimpleModalComponent } from '../simple-modal/simple-modal.component';
import { MatDialog } from '@angular/material/dialog';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[modal-trigger]',
})
export class ModalTriggerDirective implements OnInit {
  @Input('modal-trigger') modalId: string;
  @Input() template: ElementRef;
  @Input() title: string;

  el: HTMLElement;

  constructor(private ref: ElementRef, @Inject(JQUERY_TOKEN)  private $: any, private dialog: MatDialog) {
    this.el = ref.nativeElement;
  }

  ngOnInit() {
    this.el.addEventListener('click', () => {
      this.openSearchSessionDialog();
    });
  }

  private openSearchSessionDialog() {
    const dialogRef = this.dialog.open(SimpleModalComponent, {
      width: '800px',
      data: {
        title: this.title,
        template: this.template,
        elementId: this.modalId,
      }
    });

    dialogRef.afterOpened().subscribe(() => {
      console.log(this.$(`#${this.modalId}`));
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
