import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-collapsible',
  template: `
    <div (click)="toggleContent()">
      <ng-content select="[collapse-title]"></ng-content>
      <ng-content select="[collapse-body]" *ngIf="visible"></ng-content>
    </div>
  `
})

export class CollapsibleComponent implements OnInit {
  visible = true;

  constructor() { }

  ngOnInit() { }

  toggleContent() {
    this.visible = !this.visible;
  }
}
