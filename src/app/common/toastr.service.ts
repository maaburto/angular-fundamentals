import { InjectionToken } from '@angular/core';

export interface Toastr {
  success(msg: string, title?: string, options?: any): void;
  info(msg: string, title?: string, options?: any): void;
  warning(msg: string, title?: string, options?: any): void;
  error(msg: string, title?: string, options?: any): void;
}

export let TOASTR_TOKEN = new InjectionToken<Toastr>('toastr');
