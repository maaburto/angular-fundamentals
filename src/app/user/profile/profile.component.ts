import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators, Form } from '@angular/forms';
import { AuthService } from 'src/app/user/login/auth.service';
import { TOASTR_TOKEN, Toastr } from 'src/app/common';

@Component({
  templateUrl: 'profile.component.html',
  styles: [`
    .example-container .mat-form-field + .mat-form-field {
      margin-left: 8px;
    }
  `]
})
export class ProfileComponent implements OnInit {
  public mouseOverLogin: boolean;
  public isDirty = true;
  public name: string;

  firstName: FormControl;
  lastName: FormControl;
  profileForm: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    @Inject(TOASTR_TOKEN) private toastr: Toastr,
  ) {}

  ngOnInit() {
    const {
      currentUser
    } = this.authService;
    this.firstName = new FormControl(
      currentUser.firstName,
      [Validators.required, Validators.pattern('[a-zA-Z].*')]
    );
    this.lastName = new FormControl(currentUser.lastName, Validators.required);

    this.profileForm = new FormGroup({
      firstName: this.firstName,
      lastName: this.lastName
    });
  }

  validateFields(field: string): boolean {
    return this.profileForm.controls[field].invalid && this.profileForm.controls[field].touched;
  }

  save(eventForm: any) {
    if (this.profileForm.valid) {
      this.authService.updateCurrentUser(eventForm.firstName, eventForm.lastName);
      this.isDirty = false;
      this.toastr.success('Profile Saved!', 'Miracle Max Says', {
        showMethod: 'slideDown',
        hideMethod: 'fadeOut',
      });
      this.router.navigate(['/events']);
    }
  }

  logout() {
    this.authService.logout().subscribe(() => {
      this.router.navigate(['/user/login']);
    });
  }

  cancel() {
    this.router.navigate(['/events']);
  }
}
