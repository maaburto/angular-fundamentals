import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';

import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';

import userRoutes from './user.routes';

@NgModule({
  declarations: [
    ProfileComponent,
    LoginComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(userRoutes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
  ],
  bootstrap: []
})

export class UserModule { }
