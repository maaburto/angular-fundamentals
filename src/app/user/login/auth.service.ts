import { Injectable } from '@angular/core';
import { IUser } from '../shared/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class AuthService {
  currentUser: IUser;
  constructor(private http: HttpClient) { }

  loginUser(username: string, password: string) {
    const body = { username, password };
    const options = { headers: new HttpHeaders(this.setContentType('application/json')) };

    return this.http.post('/api/login', body, options)
      .pipe(tap((data) => {
        // tslint:disable-next-line: no-string-literal
        this.currentUser = data['user'] as IUser;
      }))
      .pipe(catchError(err => {
        return of(false);
      }));
  }

  logout() {
    this.currentUser = undefined;
    const options = { headers: new HttpHeaders(this.setContentType('application/json')) };

    return this.http.post('/api/logout', {}, options);
  }


  updateCurrentUser(firstName: string, lastName: string) {
    this.currentUser.firstName = firstName;
    this.currentUser.lastName = lastName;
  }

  isAuthenticated() {
    return !!this.currentUser;
  }

  checkAuthenticationStatus() {
   this.http.get('/api/currentIdentity')
    .pipe(
      tap(data => {
        if (data instanceof Object) {
          // tslint:disable-next-line: no-string-literal
          this.currentUser = data as IUser;
        }
      })
    ).subscribe();
  }

  private setContentType(value: string): { [ key: string]: any } {
    return {
      'Content-Type': value,
    };
  }
}
