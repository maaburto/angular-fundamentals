import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  username: string;
  password: string;
  mouseOverLogin: boolean;
  loginInvalid = false;

  constructor(private authService: AuthService, private router: Router) {}

  login(loginForm: NgForm) {
    const {
      value: {
        username,
        password
      }
    } = loginForm;
    this.authService.loginUser(username, password).subscribe(resp => {
      if (!resp) {
        this.loginInvalid = true;
      } else {
        this.router.navigate(['events']);
      }
    });
  }

  cancel() {
    this.router.navigate(['events']);
  }
}
