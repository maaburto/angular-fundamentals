import { Component, OnInit, Inject, OnChanges } from '@angular/core';
import { EventService } from '../shared/events.service';
import { TOASTR_TOKEN, Toastr } from 'src/app/common';
import { Router } from '@angular/router';
import { IEvent } from '../shared';
import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: 'event-create.component.html',
  styleUrls: ['event-create.component.css'],
})
export class EventCreateComponent implements OnInit {
  public newEvent: any;
  public isDirty = true;
  public events: IEvent[] = [];

  constructor(
    private eventService: EventService,
    @Inject(TOASTR_TOKEN) private toastr: Toastr,
    private router: Router,
  ) {}

  ngOnInit() {
    this.toastr.info('Hi Person!!!', 'Event Page');
  }

  saveEvent(formValue: IEvent) {
    this.eventService.saveEvent(formValue).subscribe(() => {
      this.isDirty = false;
      this.router.navigate(['/events']);
    });
  }

  cancel() {
    this.router.navigate(['/events']);
  }
}
