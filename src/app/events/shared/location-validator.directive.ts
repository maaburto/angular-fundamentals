import { Directive } from '@angular/core';
import { Validator, FormGroup, NG_VALIDATORS } from '@angular/forms';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[validateLocation]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: LocationValidator,
    multi: true,
  }]
})

// tslint:disable-next-line: directive-class-suffix
export class LocationValidator implements Validator {
  validate(formGroup: FormGroup): { [key: string ]: any  } {
    const addressControl = formGroup.controls.address;
    const cityControl = formGroup.controls.city;
    const countryControl = formGroup.controls.country;
    const onlineControl = ( formGroup.root as FormGroup).controls.onlineUrl;

    const validLocations = (
      addressControl && addressControl.value &&
      cityControl && cityControl.value &&
      countryControl && countryControl.value
    );

    const validOnlineUrl = (
      onlineControl && onlineControl.value
    );

    if (validLocations || validOnlineUrl) {
      return null;
    } else {
      return { ValidateLocation: false };
    }
  }
}
