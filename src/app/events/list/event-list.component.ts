import { Component, OnInit } from '@angular/core';
import { EventService } from '../shared/events.service';
import { Toastr } from '../../common/toastr.service';
import { ActivatedRoute } from '@angular/router';
import { IEvent } from '../shared';

@Component({
  templateUrl: './event-list.component.html'
})
export class EventListComponent implements OnInit {
  events: IEvent[] = [];

  constructor(
    private eventService: EventService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.events = this.route.snapshot.data.events;
  }

  // handledEventClicked(data) {
  //   console.log('From child component we got: ');
  //   console.log(data);
  // }
}
