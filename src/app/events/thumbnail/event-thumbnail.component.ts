import { Component, Input } from '@angular/core';
import { IEvent } from '../shared';

@Component({
  selector: 'app-event-thumbnail',
  templateUrl: './event-thumbnail.component.html',
  styleUrls: ['./event-thumbnail.component.css']
})
export class EventThumbnailComponent {
  @Input() event: IEvent;
//   @Output() eventClick = new EventEmitter();
//   public someProperty = 'Some value';

//   handleClickMe() {
//     this.eventClick.emit(this.event);
//   }

//   logFoo() {
//     console.log('Foo!');
//   }

  getStartTimeClass() {
    if (this.event && this.event?.time === '8:00 am') {
      return ['green', 'bold'];
    } else {
      return ['gray'];
    }
  }
}
