import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ISession } from '../../shared';
import { catchError } from 'rxjs/operators';
import { handleError } from 'src/app/errors/generic-error';

@Injectable()
export class VoterService {
  constructor(private http: HttpClient) { }

  deleteVoter(eventId: number, session: ISession, voterName: string) {
    const url = `/api/events/${eventId}/sessions/${session.id}/voters/${voterName}`;
    this.http.delete(url)
      .pipe(
        catchError(handleError<ISession>('addVoter'))
      ).subscribe((data: ISession) => {
        console.log(data);
      });
  }

  addVoter(eventId: number, session: ISession, voterName: string) {
    const options = { headers: new HttpHeaders(this.setContentType('application/json')) };
    const url = `/api/events/${eventId}/sessions/${session.id}/voters/${voterName}`;

    this.http.post(url, {}, options)
    .pipe(
      catchError(handleError<ISession>('addVoter'))
    ).subscribe((data: ISession) => {
      console.log(data);
    });
  }

  private setContentType(value: string): { [ key: string]: any } {
    return {
      'Content-Type': value,
    };
  }
}
