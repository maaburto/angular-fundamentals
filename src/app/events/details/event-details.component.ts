import { Component, OnInit, Input } from '@angular/core';
import { EventService } from '../shared/events.service';
import { ActivatedRoute, Params } from '@angular/router';
import { IEvent, ISession } from '../shared';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styles: [`
    .event-image { height: 100px; }
    mat-grid-list div > a { font-weight: bold; color: red; cursor: pointer }
    .filter-buttons {
      margin-right: 0.5em;
    }
  `]
})
export class EventDetailsComponent implements OnInit {
  addSessionMode: boolean;
  event: IEvent;
  filterBy = 'all';
  sortBy = 'name';

  constructor(private eventService: EventService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.forEach((param: Params) => {
      this.eventService.getEvent(+param.id).subscribe((event: IEvent) => {
        this.event = event;
        this.addSessionMode = false;
      });
    });
  }

  addSession() {
    this.addSessionMode = true;
  }

  cancelNewSession() {
    this.addSessionMode = false;
  }

  saveNewSession(session: ISession) {
    const maxId = Math.max.apply(null, this.event.sessions.map(s => s.id));
    session.id = maxId + 1;
    this.event.sessions.push(session);
    this.eventService.saveEvent(this.event).subscribe();
    this.addSessionMode = false;
  }
}
