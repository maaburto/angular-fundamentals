import { Component, Input, OnChanges } from '@angular/core';
import { ISession } from 'src/app/events/shared';
import { VoterService } from '../voter';

@Component({
  selector: 'app-session-list',
  templateUrl: 'session-list.component.html',
  styles: [`
    mat-card {
      margin: 1em;
    }

    mat-card-header {
      margin-left: -2em;
    }

    mat-card-content {
      margin-left: 1em;
    }

    .active {
      border: 1px solid red;
    }
  `]
})

export class SessionListComponent implements OnChanges {
  @Input() eventId: number;
  @Input() sessions: ISession[];
  @Input() filterBy: string;
  @Input() sortBy: string;
  visisbleSessions: ISession[] = [];

  constructor(private voterService: VoterService) {}

  // Add Voter
  // const sess: ISession = {
  //   id: this.eventId,
  //   abstract: '',
  //   duration: 1,
  //   level: '',
  //   name: '',
  //   presenter: '',
  //   voters: []
  // };

  // this.voterService.addVoter(1, sess, 'maaburto');

  // Delete Voter
  // const sess: ISession = {
  //   id: this.eventId,
  //   abstract: '',
  //   duration: 1,
  //   level: '',
  //   name: '',
  //   presenter: '',
  //   voters: []
  // };

  // this.voterService.deleteVoter(1, sess, 'maaburto');

  ngOnChanges() {
    if (this.sessions) {
      this.filterSessions(this.filterBy);
      this.sortBy === 'name' ?
        this.visisbleSessions.sort(sortByNameAsc) : this.visisbleSessions.sort(sortByVotersDesc);
    }
  }

  filterSessions(filter: string) {
    if (filter === 'all') {
      this.visisbleSessions = this.sessions.slice(0);
    } else {
      this.visisbleSessions = this.sessions.filter(session => {
        return session.level.toLocaleLowerCase() === filter;
      });
    }
  }
}

function sortByNameAsc(s1: ISession, s2: ISession) {
  if (s1.name > s2.name) {
    return 1;
  } else if (s1.name === s2.name){
    return 0;
  } else {
    return -1;
  }
}

function sortByVotersDesc(s1: ISession, s2: ISession) {
  return s2.voters.length - s2.voters.length;
}
