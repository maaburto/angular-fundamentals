import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  Router
} from '@angular/router';
import { EventService } from '../shared/events.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class EventRouteActivatorService implements CanActivate {
  constructor(private eventService: EventService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const eventId = +route.params.id;

    return this.eventService.getEvent(eventId).pipe(
      map(event => {
        const eventExists = !!event;

        if (!eventExists) {
          this.router.navigate(['/404']);
        } else {
          return eventExists;
        }
      })
    );
  }
}
