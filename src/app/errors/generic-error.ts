import { Observable, of } from 'rxjs';

export function handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
    const dataError = {
      error, operation
    };

    console.error(dataError);
    console.error(result);

    return of(result as T);
  };
}
