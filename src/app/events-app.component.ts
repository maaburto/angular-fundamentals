import { Component, OnInit } from '@angular/core';
import { AuthService } from './user/login/auth.service';

@Component({
  selector: 'app-events',
  template: `
  <app-nav-bar></app-nav-bar>
  <router-outlet></router-outlet>
  `
})
export class EventsAppComponent implements OnInit {
  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.authService.checkAuthenticationStatus();
  }
}
