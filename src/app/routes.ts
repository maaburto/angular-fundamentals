import { Routes } from '@angular/router';
import {
  EventListComponent,
  EventDetailsComponent,
  EventCreateComponent,
  EventRouteActivatorService,
  EventListResolverService,
  CreateSessionComponent
} from './events';
import { Error404Component } from './errors/404.component';

const routes: Routes = [
  {
    path: 'events',
    component: EventListComponent,
    resolve: {
      events: EventListResolverService,
    }
  },
  {
    path: 'events/new',
    component: EventCreateComponent,
    canDeactivate: ['canDeactivateCreateEvent'],
  },
  {
    path: 'events/:id',
    component: EventDetailsComponent,
    canActivate: [EventRouteActivatorService]
  },
  {
    path: 'events/session/new',
    component: CreateSessionComponent,
  },
  {
    path: 'user',
    loadChildren: async () => (await import('./user/user.module')).UserModule,
  },
  {
    path: '404',
    component: Error404Component
  },
  {
    path: '',
    redirectTo: '/events',
    pathMatch: 'full'
  },
];

export default routes;
