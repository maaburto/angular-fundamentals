/* tslint:disable:no-string-literal */

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { EventsAppComponent } from './events-app.component';

import {
  EventListComponent,
  EventDetailsComponent,
  EventCreateComponent,
  EventThumbnailComponent,
  EventRouteActivatorService,
  EventListResolverService,
  EventService,
  CreateSessionComponent,
  SessionListComponent,
  DurationPipe,
  LocationValidator,
  VoterService,
} from './events';

import { Error404Component } from './errors/404.component';
import { NavBarComponent } from './nav/nav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Services
import { AuthService } from './user/login/auth.service';

import {
  CollapsibleComponent,
  SimpleModalComponent,

  // Injector Custom Services
  TOASTR_TOKEN,
  Toastr,
  JQUERY_TOKEN,

  ModalTriggerDirective
} from './common';

const toastr: Toastr = window['toastr'];
const jQuery: any = window['$'];

// Routes
import Routes from './routes';

@NgModule({
  declarations: [
    EventsAppComponent,
    EventListComponent,
    EventThumbnailComponent,
    NavBarComponent,
    EventDetailsComponent,
    EventCreateComponent,
    CreateSessionComponent,
    SessionListComponent,
    Error404Component,
    CollapsibleComponent,
    SimpleModalComponent,
    DurationPipe,
    LocationValidator,
    ModalTriggerDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(Routes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    EventService,
    VoterService,
    {
      provide: TOASTR_TOKEN,
      useValue: toastr,
    },
    {
      provide: JQUERY_TOKEN,
      useValue: jQuery,
    },
    EventRouteActivatorService,
    {
      provide: 'canDeactivateCreateEvent',
      useValue: checkDirtyState,
    },
    EventListResolverService,
    AuthService,
  ],
  bootstrap: [EventsAppComponent],
})
export class AppModule {}

type eventDirtyTypeComponents = EventCreateComponent;
export function checkDirtyState(component: eventDirtyTypeComponents) {
  if (component.isDirty) {
    return window.confirm(
      'You have not set this event, do you really want to cancel?'
    );
  } else {
    return true;
  }
}
